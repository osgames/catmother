#include "GameWindow.h"
#include <util/ExProperties.h>
#include <util/File.h>
#include <util/FileInputStream.h>
#include <util/FileOutputStream.h>
#include <util/DirectoryInputStreamArchive.h>
#include <sg/Context.h>
#include <mem/Group.h>
#include <util/Array.h>
#include <util/Debug.h>
#include <util/System.h>
#include <util/Exception.h>
#include <stdio.h>

#include <mem/raw.h>

static char			PROP_FILE_NAME[]	= "Game.ini";
static const float	FIRST_UPDATE_DT		= 1.f / 10.f;

/* Build number of the game. */
#define BUILD_NUMBER 65

static void run( HINSTANCE hInstance )
{
	char wndTitle[13] = "Dead Justice";
	P(GameWindow) wnd = 0;

	try
	{
		// A 2mb frame allocator buffer should do the trick
		mem_framealloc_set( 2048 * 2048 );

		// load properties
		P(util::ExProperties) cfg = new util::ExProperties;
		util::FileInputStream propIns( PROP_FILE_NAME );
		cfg->load( &propIns );
		propIns.close();

		// open file archive
		P(util::InputStreamArchive) arch = 0;
		const char* paths[] = 
		{ 
			"cameras",
			"characters", 
			"particles", 
			"levels",
			"misc", 
			"fonts", 
			"sounds",
			"onscreen",
			"projectiles",
			"weapons",
			0	
		};
		
		

		util::String dataPath = "data";
		P(util::DirectoryInputStreamArchive) dirArchive = new util::DirectoryInputStreamArchive;
		dirArchive->addPath( dataPath );
		
		for ( int i = 0 ; paths[i] ; ++i )
			dirArchive->addPath( util::File(dataPath,paths[i]).getPath() );
			
		arch = dirArchive.ptr();

		// create main window
		wnd = new GameWindow( arch, cfg );
		wnd->init( wndTitle, hInstance );

		// primary main loop
		bool firstUpdate = true;
		bool wasActive = true;
		long prevTime = util::System::currentTimeMillis();

		while ( win::Window::flushWindowMessages() )
		{
			// time elapsed from last update
			long curTime = util::System::currentTimeMillis();
			long deltaTime = curTime - prevTime;
			prevTime = curTime;
			float dt = (float)deltaTime * 1e-3f;

			if ( dt > 1.f/5.f )
				dt = 1.f/5.f;

			// update game window
			if ( wnd->active() )
			{
				if ( !wasActive )
					wnd->focusLost();

				// constant update interval on first update
				if ( firstUpdate )
				{
					dt = FIRST_UPDATE_DT;
					firstUpdate = false;
				}

				if ( !wnd->update(dt) )
					break;

				wnd->render();
			}
			else if ( wasActive )
			{
			}

			wasActive = wnd->active();
			mem_framealloc_setwatermark(0);
		}

/*		util::FileOutputStream out( PROP_FILE_NAME );
		cfg->store( &out );
		out.close();*/	

		// deinit rendering context, threads, etc.
		wnd->deinit();
	}
	catch ( util::Throwable& e )
	{
		// deinit rendering context, threads, etc.
		if ( wnd )
			wnd->deinit();

		mem_framealloc_set(0);

		// minimize window so that it doesn't overlap message box
		HWND hwnd = (wnd ? wnd->handle() : 0);
		ShowCursor( TRUE );

		if ( hwnd )
			MoveWindow( hwnd, 0, 0, 4, 4, TRUE );

		// show error message
		char msgText[2560];
		char msgTitle[256];
		sprintf( msgTitle, "%s - Error", wndTitle );
		e.getMessage().format().getBytes( msgText, sizeof(msgText), "ASCII-7" );
		MessageBox( hwnd, msgText, msgTitle, MB_OK|MB_ICONERROR );
	}

	// Don't forget to free the frame allocator memory
	mem_framealloc_set(0);
}

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE, LPSTR /*cmdLine*/, int /*cmdShow*/ )
{
	run( hInstance );
	mem_printAllocatedBlocks();
	return 0;
}