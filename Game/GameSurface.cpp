#include "GameSurface.h"
#include "config.h"

//-----------------------------------------------------------------------------

GameSurface::GameSurface( script::VM* vm, util::InputStreamArchive* arch, const util::String& typeName ) :
	GameScriptable( vm, arch, 0, 0 )
{
	setName( typeName );
}
