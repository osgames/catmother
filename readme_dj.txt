Dead Justice -- 3D Action/Adventure Game Prototype

Copyright (C) 2002-2003 Cat Mother, Ltd.
See below for license information.


Briefly
-------
Game development company Cat Mother Ltd. (www.catmother.com) has now closed 
its  offices, but in their last meeting the company board decided to 
publish all company  source code as open source. Also large part of the 
content is published. Published  material includes fully playable prototype 
of a 3rd person action/adventure game and commercial quality in-house 
3D-engine (C++/DirectX9). The source code is  published under BSD license 
and the content is published under GPL license. All  material can be 
downloaded from catmother.sourceforge.net.


About This Package
------------------
This package contains executable Dead Justice game prototype and associated 
data files. For source code release please download cm-src-YYYYMMDD.zip from 
catmother.sourceforge.net. Example 3dsmax files and other content can be 
found from cm-gfx-YYYYMMDD.zip, also at catmother.sourceforge.net.

Dead Justice game prototype is mainly released for educational purposes. 
Feel free to experiment with it. 3dsmax5 exporter tool (needed to get 
3D-models into the game from 3dsmax) is provided with this package, see 
maxexport/sgexport.txt for exporter documentation. Example 3dsmax content 
can be found from cm-gfx-YYYYMMDD.zip package at catmother.sourceforce.net.


License
-------
This program is free software; you can redistribute it and/or modify it 
under the terms of the GNU General Public License as published by the Free 
Software Foundation; either version 2 of the License, or (at your option) 
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT 
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or 
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for 
more details.

You should have received a copy of the GNU General Public License 
(see licence_dj.txt) along with this program; if not, write to the Free 
Software Foundation, Inc., 59 Temple Place, Suite 330, Boston, 
MA 02111-1307 USA


Notes About Running This Demo
-----------------------------

- Demo currently requires a display card with pixel shader support.

- Microsoft DirectX 9 run time needs to be installed.
  (can be downloaded from www.microsoft.com/directx)

- This PC demo tries to emulate visually Xbox on PC and supports PS2 
  dualshock controller via EMS2 adapter to provide console style controls.

- Demo has been tested to run on GeForce3, GeForce4 and ATI Radeon 9700 Pro
  with latest NVidia/ATI reference display drivers (April 9, 2003).
  If you encounter problems with these cards make sure you have the latest
  display drivers installed.
  GeForce MX/Go cards are not supported as they are not equipped with pixel 
  shader support.

- The demo has been tested on Windows XP and Windows 2000.


Recommended System Configuration
--------------------------------

Processor:
P4 1500+ Mhz

Memory:
256MB RAM

Graphics card:
NVIDIA G-Force 3 or 4 graphics card with 64MB or more memory.


PS2 Dualshock Joypad
--------------------

For closest to console playability experience, PS2 joypad is also supported.
EMS2 PS2 adapter with a PS2 dualshock controller can be used for best gameplay
experience. All game controls are mapped to PS2 dualshock game controller.

EMS2 adapters can be bought from various hardcore gaming accesory shops like:

- Lik Sang (http://www.liksang.com/)

- ZTNet store (http://secure.fragism.com/store/)


How To Play
-----------

Game starts with a short intro sequence, which can be skipped by pressing
"space" key.

Actual gameplay starts after intro is finished.

In this demo, you control Harley, other of the two main playable characters in
the game.

Game starts with a quick interactive tutorial which introduces all moves you
can perform in this demo. For the first time it's suggested that you follow
through the quick tutorial and in later game sessions you can skip it by walking
through the hole in the fence to the area of action.

------------------------------------------------------------------------------

In-Game Functions

Generic Controls
----------------

F8              Toggle fly camera / PAUSE , can be used to rotate around
                character and view environment. Use W and S to move camera
                closer to / further away from character.
                
T               Toggle slow motion mode on / off
                
I               Toggle invulnerability cheat mode on / off

CTRL            Skip intro / cut scene

F11             Restart demo

ESC             During gameplay to exit demo and return to desktop


Player Controls (Keyboard / Mouse)
--------------------------------

MOUSE           Move crosshair / turn character

MOUSE LEFT      Aim / shoot

MOUSE RIGHT     Physical attack strike

LEFT CONTROL    Physical attack kick

W A S D         Move character

C               Crouch

Q E             Peek left / right to quickly glance around a corner

X               Roll

R               Reload weapon, can also be performed when weapon is half-empty

TAB             Cycle weapon

Z               Throw an empty shell to distract enemies

LEFT SHIFT      Sneak modifier, press and hold to make character movement
                sneaking

CAPSLOCK        Toggle between run/walk character movement


Player Controls (PS2 Dualshock Pad)
-----------------------------------

L THUMB STICK   Move character, sneak, walk and run with analog control

R THUMB STICK   Move crosshair / turn character

R2              Aim / shoot

L2              Crouch

L1              Roll, combined with LEFT THUMB STICK performs roll to
                different directions
                    
DIGITAL UP      Cycle weapon

DIGITAL L/R     Peek left / right to quickly glance around a corner

SQUARE          Physical attack punch

CROSS           Physical attack kick

TRIANGLE        Reload weapon, can also be performed when weapon is
                half-empty

R1              Throw an empty shell to distract enemies

------------------------------------------------------------------------------

Gameplay Features

Basic Player Movement
---------------------

 Harley can sneak, walk and run with seamless analog speed control if you're
using a PS2 controller. You can move to all directions, without limits.
Only limitation is that you cannot run or walk so fast backwards.
This means that you can move sideways, diagonal directions etc.

 Analog control is simulated on keyboard with modifier keys to alter movement
speed. Movement speed is essential to avoid certain enemies / assault them
by suprise.


Turning and Aiming
------------------

 When you move mouse / use left thumb stick with a PS2 controller, you can move
your aim freely - Where the crosshair points, Harley aims.

 When you move crosshair enough off-center, Harley starts to turn. The more you
move crosshair off-center, Harley will turn more quickly.
 
 When you return crosshair to roughly center, Harley stops turning. This method
provides very easy and smooth 1st person-like control and freedom.


Shooting and Reloading
----------------------

 Harley can raise weapon to shooting position when you're certain that you'll
meet resistance in next few seconds - This way you save a few precicious
seconds. Just click fire button once and Harley raises weapon to shooting
position.

 Harley can reload your weapon whenever you want. This way you have control of
the amount of ammo you have when you jump out of cover and fire at enemies.

You can cancel reloading of weapon if you're in desperate situation and need to
run for cover. You can do this automaticly by rolling to any of directions
possible.


Peeking Behind Corners
----------------------

 You can assault enemies more safely if you use peek mode - Harley can quickly
"peek" around a corner by hopping to a wide, low stance to 2m sideways
location, left or right. You can aim fire freely in peek mode too. When you
release peek button, Harley returns behind corner.


Crouched Movement
-----------------

 Harley can crouch to take cover behind boxes, fences and so on.

 Harley can also move in crouched position. This might sometimes give you bit
more protection if you need to move when enemies are shooting at you and
you're moving behind medium height objects.


Rolling
-------

 Harley can roll forwards, backwards and sideways on ground in case you need to
take cover quickly / avoid enemy fire if you're taken by surprise.


Weapons
-------

 Demo has currently three different weapons, 9mm automatic pistol, pump action
shotgun and an assault rifle.

 Weapons have different characteristics - Each weapon has different effective
range, accuracy, damage and firing rate.


Stealth
-------

 You can use shadows to your advantage - More dark shadow you're in, less
probable it is that enemies can detect you.

 Too much noise will alert the enemies, so it's sometimes better avoid shooting.

 The faster you move, more noise you make.

 Be also aware that different materials have certain noise level - Footsteps
ring on metal quite loudly, while walking on sand is much more quiet.

 The closer you are behind enemy, more clearly they hear you.


Better Ways to Beat Enemies
---------------------------

 Harley can throw empty shells to distract enemies and then attack them when you
got a good sight.

 If you approach enemy behind, unheard, you can knock him out without making too
much noise. Keep in mind that some enemies are more wide awake than others.

 There are quite a few dynamic elements in the demo level that you can exploit
to beat the enemies more easily - Look for welding gas bottles, fire
extinguishers, wooden crates and a few other items. These items that contain
explosive materials might explode after a few shots, taking down enemies nearby.

------------------------------------------------------------------------------

Technical Features Shown in Dead Justice Demo
---------------------------------------------

Graphical Features
------------------

- DirectX9 support, with support for HLSL and latest graphical effects.

- Different materials are simulated with pixel shaders:
  - Cubic environment reflections.
  - Skin shading with skydome illumination.
  - Metals with fresnel reflection for added realism.
  - Normal, specular and bump mapping used on various items to add more detail
    to medium/high resolution meshes. Weapons and parts motorcycle of use this
    method.

- Normal, specular and bump mapping in characters, more details are created with
  normal maps.

- Characters are dynamically light sourced, and normal maps lit accordingly.

- Characters also take environment illumination level in account, so characters
  are more dark in shadows and so on.

- Characters cast volumetric stencil buffer shadows properly on environment
  objects.

- Volumetric shadows are also light sourced, this can be seen well inside the
  storage building.

- Facial animation (blended vertex morphing)


Scripting
---------

- Scriptable Particle systems - All particle systems in demo are scripted.

- Scriptable cut scenes - Intro and cutscene sequences are completely scripted,
  artist controlled effects, audio and animation.

- Scriptable enemy behaviour - Enemy AI combat, movement and other behavior is
  built mainly with scripting.


Sound Features
--------------

- 3D Audio.

- Material modeling (see next chapter).


Material modeling
-----------------

- Material modeling takes in account both audio and also how enemies react to
  you - In addition to having different audio reaction when shot / walked on,
  materials also cause noise in game world according to material definitions.

- All surface materials can have different material properties - Metallic
  materials clank and ring when you walk on them, sand grumbles under your
  boots and so on.

- Some materials can be shot through, so bullets won't stop to chainlink fences
  etc.

- Materials have different particle effect / audio reactions - Shooting metal
  throws sparks, stone chips fly when you shoot concrete, wood pieces fall
  when you shoot wooden fences.

------------------------------------------------------------------------------  
  
Dead Justice Game Prototype Credits
-----------------------------------
Game design....... Olli Sorjonen, Sami Sorjonen
Programming....... Jani Kajala (lead), Toni Aittoniemi
Graphic content... Olli Sorjonen, Sami Sorjonen
Level scripting... Olli Sorjonen, Jani Kajala, Toni Aittoniemi
Testing........... Olli Sorjonen, Jani Kajala, Toni Aittoniemi

Dead Justice game prototype uses in-house core technology and 3D-engine,
developed by Jani Kajala.
