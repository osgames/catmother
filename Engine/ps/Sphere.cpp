#include "Sphere.h"
#include <util/Math.h>
#include <assert.h>

using namespace util;
using namespace math;

namespace ps
{
	Sphere::Sphere( const Vector3& pos, float r )
	{
		m_pos = pos;
		m_r = r;
	}

	void Sphere::getRandomPoint( Vector3* point )
	{
		// axis by axis
		float r = m_r;
		float x = Math::random()*2.f - 1.f;
		float d2 = 1.f - x*x;
		if ( d2 < 0.f )
			d2 = 0.f;
		float d = Math::sqrt( d2 );
		float y = (Math::random()*2.f - 1.f) * d;
		d2 = 1.f - x*x - y*y;
		if ( d2 < 0.f )
			d2 = 0.f;
		d = Math::sqrt( d2 );
		float z = (Math::random()*2.f - 1.f) * d;
		*point = m_pos;
		*point += Vector3( x*r, y*r, z*r );
	}
}