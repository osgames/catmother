#include "Integer.h"
#include "NumberParse.h"
#include <limits>

#undef max
#undef min

namespace util
{
	int Integer::MAX_VALUE = std::numeric_limits<int>::max();
	int Integer::MIN_VALUE = std::numeric_limits<int>::min();

	int Integer::parseInt( const String& str )
	{
		return NumberParse<int>::parse( str );
	}
}