#ifndef _MEM_ALLOCATOR_H
#define _MEM_ALLOCATOR_H

#include <mem/Group.h>
#include <new>

namespace util
{
	/** 
	 * Manager for storage allocation of objects of type T. 
	 * Supports named memory block groups (for memory statistics).
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	template <class T> class Allocator
	{
	public:
		/** Creates allocator. */
		Allocator();

		/** Creates allocator of specified user. */
		explicit Allocator( const char* file, int line=-1 );

		/** Releases allocator. */
		~Allocator();

		/** Copy by reference. */
		Allocator( const Allocator<T>& other );

		/** Copy by reference. */
		Allocator<T>&	operator=( const Allocator<T>& other );

		/** Allocates and constructs n objects. */
		T*				allocate( int n, void* hint=0 );

		/** Destructs and deallocates n objects. */
		void			deallocate( T* p, int n );

		/** Constructs object to uninitialized memory. */
		T*				construct( void* p, const T& v );
		
		/** Destructs object. */
		void			destroy( T* p );

		/** Returns number of bytes memory allocated in the group. */
		int 			bytesInUse() const;

		/** Returns number of memory blocks allocated in the group. */
		int 			blocksInUse() const;

		/** Returns name of the allocator group. */
		const char* 	name() const;

	private:
		void* m_group;
	};

	template <class T> Allocator<T>::Allocator()
	{
		m_group = mem_Group_create(__FILE__,__LINE__);
	}

	template <class T> Allocator<T>::Allocator( const char* file, int line )
	{
		m_group = mem_Group_create(file,line);
	}

	template <class T> Allocator<T>::~Allocator()
	{
		mem_Group_release(m_group);
	}

	template <class T> Allocator<T>::Allocator( const Allocator<T>& other )
	{
		m_group = mem_Group_copy( other.m_group );
	}

	template <class T> Allocator<T>& Allocator<T>::operator=( const Allocator<T>& other )
	{
		void* group = mem_Group_copy( other.m_group );
		mem_Group_release(m_group);
		m_group = group;
		return *this;
	}

	template <class T> T* Allocator<T>::allocate( int n, void* )
	{
		void* mem = mem_Group_allocate( m_group, sizeof(T)*n );
		T* item0 = reinterpret_cast<T*>(mem); 
		int i = 0;

		try
		{
			T* item = item0; 
			for ( ; i < n ; ++i ) 
			{
				void* itemmem = item; 
				::new(itemmem) T;
				++item;
			} 
			return item0;
		}
		catch ( ... )
		{
			--i;
			T* item = item0 + i;
			for ( ; i >= 0 ; --i ) 
			{
				item->~T();
				--item;
			} 
			mem_Group_free( m_group, mem, sizeof(T)*n );
			throw;
		}
	}

	template <class T> void Allocator<T>::deallocate( T* p, int n )
	{
		void* mem = p;
		p += n-1;
		for ( int i = 0 ; i < n ; ++i ) 
		{
			p->~T(); 
			--p;
		}
		mem_Group_free( m_group, mem, sizeof(T)*n );
	}

	template <class T> T* Allocator<T>::construct( void* p, const T& v )
	{
		return ::new(p) T(v);
	}

	template <class T> void Allocator<T>::destroy( T* p )
	{
		p->~T(); 
		p = p;
	}

	template <class T> const char* Allocator<T>::name() const
	{
		return mem_Group_name( m_group );
	}

	template <class T> int Allocator<T>::bytesInUse() const
	{
		return mem_Group_bytesInUse( m_group );
	}

	template <class T> int Allocator<T>::blocksInUse() const
	{
		return mem_Group_blocksInUse( m_group );
	}
}

#endif