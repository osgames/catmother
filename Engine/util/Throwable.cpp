#include "Throwable.h"

namespace util
{
	Throwable::Throwable()
	{
		setMessage( Format("") );
	}

	Throwable::Throwable( const Format& msg )
	{
		setMessage( msg );
	}

	void Throwable::setMessage( const Format& msg )
	{
		m_msg = msg;
	}

	const Format& Throwable::getMessage() const
	{
		return m_msg;
	}

	const String& Throwable::getStackTrace() const
	{
		return m_trace;
	}
}