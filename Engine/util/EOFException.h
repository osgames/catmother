#ifndef _IO_EOFEXCEPTION_H
#define _IO_EOFEXCEPTION_H

#include <util/IOException.h>

namespace util
{
	/**
	 * Thrown if an end of file or end of stream has been reached 
	 * unexpectedly during input.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class EOFException : public IOException
	{
	public:
		EOFException( const Format& msg ) : IOException(msg)
		{
		
		}
	};
}

#endif