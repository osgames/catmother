#ifndef _LANG_DEBUG_H
#define _LANG_DEBUG_H

#include <util/Formattable.h>

namespace util
{
	/** 
	 * Debug output. Thread-safe.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class Debug
	{
	public:
		/** Outputs an informative message. */
		static void		println( const String& str );
		static void		println( const String& str, const Formattable& arg0 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8 );
		static void		println( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8, const Formattable& arg9 );

		/** Outputs a warning message. */
		static void		printlnWarning( const String& str );
		static void		printlnWarning( const String& str, const Formattable& arg0 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8 );
		static void		printlnWarning( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8, const Formattable& arg9 );

		/** Outputs an error message. */
		static void		printlnError( const String& str );
		static void		printlnError( const String& str, const Formattable& arg0 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8 );
		static void		printlnError( const String& str, const Formattable& arg0, const Formattable& arg1, const Formattable& arg2, const Formattable& arg3, const Formattable& arg4, const Formattable& arg5, const Formattable& arg6, const Formattable& arg7, const Formattable& arg8, const Formattable& arg9 );
	};
}

#endif