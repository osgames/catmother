#include "Reader.h"
#include <util/stdint.h>

namespace util
{
	Reader::Reader()
	{
	}

	Reader::~Reader()
	{
	}

	long Reader::skip( long count )
	{
		long c = 0;
		uint16_t ch;
		while ( c < count && 1 == read(&ch,1) )
		{
			++c;
		}
		return c;
	}

	void Reader::mark( int /*readlimit*/ )
	{
	}

	void Reader::reset()
	{
	}

	bool Reader::markSupported() const
	{
		return false;
	}
}