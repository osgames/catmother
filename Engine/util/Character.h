#ifndef _LANG_CHARACTER_H
#define _LANG_CHARACTER_H

#include <util/stdint.h>

namespace util
{
	/** 
	 * Immutable Char wrapper and type information.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class Character
	{
	public:
		Character()
		{
			m_v=0;
		}

		Character( uint16_t v )
		{
			m_v=v;
		}

		/** Returns true if the values are equal. */
		bool operator==( const Character& other ) const
		{
			return m_v == other.m_v;
		}

		/** Returns true if the values are not equal. */
		bool operator!=( const Character& other ) const
		{
			return m_v != other.m_v;
		}

		/** Returns the value of this object. */
		uint16_t charValue() const
		{
			return m_v;
		}

		/** Returns hash code of this object. */
		int hashCode() const
		{
			return m_v;
		}

		/** Returns true if the code point is a Unicode digit. */
		static bool isDigit( uint32_t cp );

		/** Returns true if the code point is a Unicode letter. */
		static bool isLetter( uint32_t cp );

		/** Returns true if the code point is a Unicode letter or digit. */
		static bool isLetterOrDigit( uint32_t cp );

		/** Returns true if the code point is a Unicode lower-case. */
		static bool isLowerCase( uint32_t cp );

		/** Returns true if the code point is a Unicode upper-case. */
		static bool isUpperCase( uint32_t cp );

		/** Returns true if the code point is a Unicode whitespace. */
		static bool isWhitespace( uint32_t cp );

	private:
		uint16_t m_v;
	};
}

#endif