#ifndef _IO_DIRECTORYINPUTSTREAMARCHIVE_H
#define _IO_DIRECTORYINPUTSTREAMARCHIVE_H

#include <util/InputStreamArchive.h>

namespace util
{
	/** 
	 * Class for opening input streams from multiple directories. 
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class DirectoryInputStreamArchive : public InputStreamArchive
	{
	public:
		DirectoryInputStreamArchive();

		~DirectoryInputStreamArchive();
		
		/** Adds a directory to the list of searched paths. */
		void addPath( const String& path );

		/** Adds a directory and all subdirs to the list of searched paths. */
		void addPaths( const String& path );

		/** Removes all search paths. */
		void removePaths();

		/** Closes the archive. */
		void close();

		/** 
		 * Returns input stream to specified entry. 
		 * @exception IOException
		 */
		InputStream* getInputStream( const String& name );

		/** 
		 * Returns input stream to ith entry. 
		 * @exception IOException
		 */
		InputStream* getInputStream( int index );

		/** 
		 * Returns ith entry name from the archive. 
		 * @exception IOException
		 */
		String getEntry( int index ) const;

		/** 
		 * Returns number of entries in the archive. 
		 * @exception IOException
		 */
		int size() const;

		/** Returns name of the archive. */
		String toString() const;

	private:
		class DirectoryInputStreamArchiveImpl;
		P(DirectoryInputStreamArchiveImpl) m_this;

		DirectoryInputStreamArchive( const DirectoryInputStreamArchive& );
		DirectoryInputStreamArchive& operator=( const DirectoryInputStreamArchive& );
	};
}

#endif