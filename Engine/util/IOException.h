#ifndef _IO_IOEXCEPTION_H
#define _IO_IOEXCEPTION_H

#include <util/Exception.h>

namespace util
{
	/**
	 * Thrown if input/output operation fails.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class IOException : public Exception
	{
	public:
		IOException( const Format& msg ) : Exception(msg) {}
	};
}

#endif