#include "Semaphore.h"

#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#	include <limits.h>
#	define SEM_HANDLE HANDLE
#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
#	include <semaphore.h>
#	define SEM_HANDLE sem_t
#endif

namespace util
{
	class Semaphore::SemaphoreImpl
	{
	public:
		SEM_HANDLE obj;

		SemaphoreImpl( int value )
		{
			#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
			obj = CreateSemaphore( 0, value, INT_MAX, 0 );
			#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
			sem_init( &obj, 0, value );
			#endif
		}

		~SemaphoreImpl()
		{
			#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
			CloseHandle( obj ); 
			#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
			sem_destroy( &obj );
			#endif
		}

		void wait()
		{
			#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
			WaitForSingleObject( obj, INFINITE );
			#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
			sem_wait( &obj );
			#endif
		}

		void signal()
		{
			#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
			ReleaseSemaphore( obj, 1, 0 );
			#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
			sem_post( &obj );
			#endif
		}
	};

	Semaphore::Semaphore( int value )
	{
		m_this = new SemaphoreImpl( value );
	}

	Semaphore::~Semaphore()
	{
		delete m_this;
	}

	void Semaphore::wait()
	{
		m_this->wait();
	}

	void Semaphore::signal()
	{
		m_this->signal();
	}
}