#ifndef _LANG_EXCEPTION_H
#define _LANG_EXCEPTION_H

#include <util/Throwable.h>

namespace util
{
/**
 * Base class for all exceptions that an application might want to handle.
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class Exception :
	public Throwable
{
public:
	/** Creates an exception with no error description. */
	Exception() {}

	/** Creates an exception with the specified error description. */
	Exception( const Format& msg )											: Throwable(msg) {}
};


} // lang


#endif // _LANG_EXCEPTION_H

