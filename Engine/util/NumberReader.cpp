#include "NumberReader.h"
#include <math.h>
#include <ctype.h>

namespace util
{
	/**
	 * Converts ASCII-7 digit character to number. 
	 * @returns 0 if the digit cannot be converted.
	 */
	template <class T> static T digitToNumber( char ch )
	{
		// not a digit?
		if ( !(ch >= char('0') && ch <= char('9')) )
			return 0;

		return T( ch - unsigned('0') );
	}

	/**
	 * Returns true if the character is exponent identifier 'e' of floating point value.
	 */
	static bool expChar( char ch )
	{
		// is 'E' or 'e'?
		return (ch == char('e') || ch == char('E'));
	}

	/**
	 * Returns true if the character is ASCII-7 whitespace.
	 */
	static bool isAsciiSpace( char ch )
	{
		return 0 != isspace(ch);
	}

	/**
	 * Returns true if the character is ASCII-7 digit.
	 */
	static bool isAsciiDigit( char ch )
	{
		return 0 != isdigit(ch);
	}

	int NumberReader<double>::put( char ch )
	{
		switch ( m_state )
		{
		case STATE_INIT:
			if ( isAsciiSpace(ch) )
				return 1;
			m_state			= STATE_SIGN;
			m_valid			= false; 
			m_sign			= 1;
			m_value			= 0;
			m_expSign		= 1;
			m_expValue		= 0;
		
		case STATE_SIGN:
			if ( char('+') == ch )
			{
				m_sign = 1;
				m_state = STATE_BODY;
				m_valid	= false; 
				return 1;
			}
			else if ( char('-') == ch )
			{
				m_sign = -1;
				m_state = STATE_BODY;
				m_valid	= false; 
				return 1;
			}
			m_state = STATE_BODY;
		
		case STATE_BODY:
			if ( char('.') == ch )
			{
				m_state = STATE_FRACTION;
				m_fractionScale = double(0.1);
				return 1;
			}
			else if ( expChar(ch) )
			{
				m_state = STATE_EXP;
				m_valid	= false; 
				return 1;
			}
			else if ( isAsciiDigit(ch) )
			{
				m_value *= double(10);
				m_value += digitToNumber<double>(ch);
				m_valid = true;
				return 1;
			}
			break;

		case STATE_FRACTION:
			if ( isAsciiDigit(ch) )
			{
				m_value += digitToNumber<double>(ch) * m_fractionScale;
				m_fractionScale *= double(0.1);
				m_valid = true;
				return 1;
			}
			else if ( expChar(ch) )
			{
				m_state = STATE_EXP;
				m_valid	= false; 
				return 1;
			}
			break;

		case STATE_EXP:
			if ( char('+') == ch )
			{
				m_expSign = 1;
				m_state = STATE_EXP_BODY;
				m_valid	= false; 
				return 1;
			}
			else if ( char('-') == ch )
			{
				m_expSign = -1;
				m_state = STATE_EXP_BODY;
				m_valid	= false; 
				return 1;
			}
			m_state = STATE_EXP_BODY;
		
		case STATE_EXP_BODY:
			if ( char('.') == ch )
			{
				m_state = STATE_EXP_FRACTION;
				m_fractionScale = double(0.1);
				return 1;
			}
			else if ( isAsciiDigit(ch) )
			{
				m_expValue *= double(10);
				m_expValue += digitToNumber<double>(ch);
				m_valid	= true; 
				return 1;
			}
			break;

		case STATE_EXP_FRACTION:
			if ( isAsciiDigit(ch) )
			{
				m_expValue += digitToNumber<double>(ch) * m_fractionScale;
				m_fractionScale *= double(0.1);
				m_valid	= true; 
				return 1;
			}
			break;
		}

		if ( !isAsciiSpace(ch) )
			m_valid = false;
		m_state = STATE_INIT;
		return 0;
	}

	double NumberReader<double>::value() const
	{
		return m_sign * m_value * (double)pow( 10.0, m_expSign*m_expValue );
	}

	int NumberReader<long>::put( char ch )
	{
		switch ( m_state )
		{
		case STATE_INIT:
			if ( isAsciiSpace(ch) )
				return 1;
			m_state			= STATE_SIGN;
			m_valid			= false; 
			m_sign			= 1;
			m_value			= 0;
		
		case STATE_SIGN:
			if ( char('+') == ch )
			{
				m_sign = 1;
				m_state = STATE_BODY;
				m_valid	= false; 
				return 1;
			}
			else if ( char('-') == ch )
			{
				m_sign = -1;
				m_state = STATE_BODY;
				m_valid	= false; 
				return 1;
			}
			m_state = STATE_BODY;
		
		case STATE_BODY:
			if ( isAsciiDigit(ch) )
			{
				m_value *= long(10);
				m_value += digitToNumber<long>(ch);
				m_valid = true;
				return 1;
			}
			break;
		}

		if ( !isAsciiSpace(ch) )
			m_valid = false;
		m_state = STATE_INIT;
		return 0;
	}

	long NumberReader<long>::value() const
	{
		return m_sign * m_value;
	}

	int NumberReader<unsigned long>::put( char ch )
	{
		switch ( m_state )
		{
		case STATE_INIT:
			if ( isAsciiSpace(ch) )
				return 1;
			m_state			= STATE_BODY;
			m_valid			= false; 
			m_value			= 0;
		
		case STATE_BODY:
			if ( isAsciiDigit(ch) )
			{
				m_value *= (unsigned long)(10);
				m_value += digitToNumber<unsigned long>(ch);
				m_valid = true;
				return 1;
			}
			break;
		}

		if ( !isAsciiSpace(ch) )
			m_valid = false;
		m_state = STATE_INIT;
		return 0;
	}

	unsigned long NumberReader<unsigned long>::value() const
	{
		return m_value;
	}
}