#include "FilterInputStream.h"
#include <assert.h>

namespace util
{
	FilterInputStream::FilterInputStream( InputStream* source ) : m_source(source) 
	{
	}

	void FilterInputStream::close()																	
	{
		m_source->close();
	}

	long FilterInputStream::read( void* data, long size )											
	{
		return m_source->read(data,size);
	}

	long FilterInputStream::skip( long n )															
	{
		assert( n >= 0 );
		return m_source->skip(n);
	}

	long FilterInputStream::available() const														
	{
		return m_source->available();
	}

	String FilterInputStream::toString() const
	{
		return m_source->toString();
	}
}