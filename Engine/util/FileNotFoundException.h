#ifndef _IO_FILENOTFOUNDEXCEPTION_H
#define _IO_FILENOTFOUNDEXCEPTION_H

#include <util/IOException.h>

namespace util
{
	/**
	 * Thrown if specified file or path is not found or 
	 * if the file cannot be opened for some other reason.
	 * (for example if the file tried to be opened is a directory)
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class FileNotFoundException : public IOException
	{
	public:
		FileNotFoundException( const Format& msg ) : IOException(msg) {}
	};
}

#endif