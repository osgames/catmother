#ifndef _IO_COMMANDREADER_H
#define _IO_COMMANDREADER_H

#include <util/Object.h>
#include <util/String.h>

namespace util
{
	class Reader;

	/** 
	 * Class for reading simple text file commands and parameters. 
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class CommandReader : public Object
	{
	public:
		///
		explicit CommandReader( Reader* reader, const String& name );

		/** 
		 * Skips whitespace characters. 
		 * @exception IOException
		 */
		void		skipWhitespace();
		
		/**
		 * Reads a whitespace-delimited string from the stream. 
		 * @return false if end-of-file occured.
		 * @exception IOException
		 */
		bool		readString( String& str );

		/**
		 * Reads a line from the stream. 
		 * Line is delimited by \n or \r or \n\r.
		 * @return false if end-of-file occured.
		 * @exception IOException
		 */
		bool		readLine( String& str );

		/**
		 * Reads a long integer using US/English locale.
		 * @exception IOException
		 */
		long		readLong();

		/**
		 * Reads an integer using US/English locale.
		 * @exception IOException
		 */
		int			readInt();

		/**
		 * Reads a floating point value using US/English locale.
		 * @exception IOException
		 */
		float		readFloat();

		/**
		 * Reads a single hex digit.
		 * @exception IOException
		 */ 
		int			readHex();

		/**
		 * Single character look-ahead.
		 * @exception IOException
		 */
		uint16_t peekChar();

		/** Returns name of the command stream. */
		const String&	name() const;

		/** Returns line number of the current read position. */
		int line() const;

	private:
		Reader*	m_reader;
		int		m_line;
		int		m_lineMark;
		String	m_name;

		bool readChar( uint16_t* ch );
		void mark1();
		void reset1();
		
		CommandReader( const CommandReader& );
		CommandReader& operator=( const CommandReader& );
	};
}

#endif