#ifndef _UTIL_EXPROPERTIES_H
#define _UTIL_EXPROPERTIES_H

#include <util/Properties.h>

namespace util
{
/** 
 * Extended properties class.
 * Contains primitive parsing operations.
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class ExProperties : public Properties
{
public:
	/** 
	 * Sets integer value property. 
	 * @param name Name of the property.
	 * @param x New value of the property.
	 */
	void setInteger( const String& name, int x );

	/** 
	 * Sets floating point value property. 
	 * @param name Name of the property.
	 * @param x New value of the property.
	 */
	void setFloat( const String& name, float x );

	/** 
	 * Sets boolean value property. 
	 * @param name Name of the property.
	 * @param x New value of the property.
	 */
	void setBoolean( const String& name, bool x );

	/** 
	 * Reads integer array value property. 
	 * @param name Name of the property.
	 * @param buffer Array of values to set.
	 * @param count Number of values in the array.
	 */
	void setIntegers( const String& name, const int* buffer, int count );

	/** 
	 * Sets floating point array value property. 
	 * @param name Name of the property.
	 * @param buffer Array of values to set.
	 * @param count Number of values in the array.
	 */
	void setFloats( const String& name, const float* buffer, int count );

	/** 
	 * Sets boolean array value property.
	 * @param name Name of the property.
	 * @param buffer Array of values to set.
	 * @param count Number of values in the array.
	 */
	void setBooleans( const String& name, const bool* buffer, int count );

	/** 
	 * Sets string array value property.
	 * @param name Name of the property.
	 * @param buffer Array of values to set.
	 * @param count Number of values in the array.
	 */
	void setStrings( const String& name, const String* buffer, int count );

	/** 
	 * Returns integer value property. 
	 * @param name Name of the property.
	 * @exception NumberFormatException
	 */
	int getInteger( const String& name ) const;

	/** 
	 * Returns floating point value property. 
	 * @param name Name of the property.
	 * @exception NumberFormatException
	 */
	float getFloat( const String& name ) const;

	/** 
	 * Returns boolean value property. 
	 * @param name Name of the property.
	 * @exception NumberFormatException
	 */
	bool getBoolean( const String& name ) const;

	/** 
	 * Reads integer array value property. 
	 * @param name Name of the property.
	 * @param buffer Array of values to get.
	 * @param count Number of values in the array.
	 * @exception NumberFormatException
	 */
	void getIntegers( const String& name, int* buffer, int count ) const;

	/** 
	 * Returns floating point array value property. 
	 * @param name Name of the property.
	 * @param buffer Array of values to get.
	 * @param count Number of values in the array.
	 * @exception NumberFormatException
	 */
	void getFloats( const String& name, float* buffer, int count ) const;

	/** 
	 * Returns boolean array value property.
	 * @param name Name of the property.
	 * @param buffer Array of values to get.
	 * @param count Number of values in the array.
	 * @exception NumberFormatException
	 */
	void getBooleans( const String& name, bool* buffer, int count ) const;

	/** 
	 * Returns string array value property.
	 * @param name Name of the property.
	 * @param buffer Array of values to get.
	 * @param count Number of values in the array.
	 * @exception Exception
	 */
	void getStrings( const String& name, String* buffer, int count ) const;
};
}

#endif