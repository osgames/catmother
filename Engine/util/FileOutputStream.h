#ifndef _IO_FILEOUTPUTSTREAM_H
#define _IO_FILEOUTPUTSTREAM_H

#include <util/OutputStream.h>
#include <util/String.h>

namespace util
{
	/**
	 * FileOutputStream writes bytes to a file in a file system.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class FileOutputStream :
		public OutputStream
	{
	public:
		/** 
		 * Opens a file output stream. 
		 *
		 * @exception FileNotFoundException
		 */
		explicit FileOutputStream( const String& filename );

		///
		~FileOutputStream();

		/**
		 * Closes the stream and releases associated resources.
		 * Closed stream cannot perform any operations and cannot be reopened.
		 */
		void	close();

		/**
		 * Forces any buffered output bytes to be written out.
		 *
		 * @exception IOException
		 */
		void	flush();

		/**
		 * Writes specified number of bytes to the stream.
		 *
		 * @exception IOException
		 */
		void	write( const void* data, long size );

		/** Returns name of the file. */
		String	toString() const;

	private:
		class FileOutputStreamImpl;
		P(FileOutputStreamImpl) m_this;

		FileOutputStream();
		FileOutputStream( const FileOutputStream& );
		FileOutputStream& operator=( const FileOutputStream& );
	};
}

#endif