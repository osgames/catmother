#ifndef _UTIL_FRAMEALLOCATOR_H
#define _UTIL_FRAMEALLOCATOR_H

#include <mem/raw.h>

/** 
 * TempAllocator is a temporary allocator which uses the frame
 * allocator functions in the mem lib.
 * It it best used in functions where you want to temporarily allocate
 * some memory to perform processing on, without constantly allocating and then
 * freeing memory using the system's memory allocation routines (which might not take
 * kind to such abuse).
 *
 * Example Usage:
 * <pre>
 * void myFunc()
 * {
 *    TempAllocator ta;
 *    char *buffer = (char*)ta.alloc(1024);
 *    Debug:println("Allocated {0} bytes", ta.size());
 *    // (ta will then be destroyed when function exits)
 * }
 * </pre>
 *
 * @author James Urquhart (jamesu@gmail.com)
 */
class TempAllocator
{
public:
	TempAllocator()
	{
		m_currentalloc = 0;
		m_watermark = mem_framealloc_getwatermark();
	}

	~TempAllocator()
	{
		mem_framealloc_setwatermark(m_watermark);
	}

	void *alloc(uint32_t size)
	{
		void *ptr = mem_framealloc_alloc(size);
		if (ptr)
			m_currentalloc += size;
		return ptr;
	}

	void reset()
	{
		mem_framealloc_setwatermark(m_watermark);
	}

	uint32_t size()
	{
		return m_currentalloc;
	}

private:
	uint32_t m_watermark;
	uint32_t m_currentalloc;
};

#endif