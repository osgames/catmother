#ifndef _UTIL_PROPERTIES_H
#define _UTIL_PROPERTIES_H

#include <util/String.h>
#include <util/Hashtable.h>

namespace util
{
	class Reader;
	class Writer;
	class OutputStream;
	class InputStream;

	/** 
	 * The Properties class represents a persistent set of properties. 
	 * The Properties can be saved to a stream or loaded from a stream. 
	 * A property file entry consists of the property name, assignment operator, and the value.
	 * Property files can contain comment lines starting with '#'.
	 *
	 * Property file example:
	 * <pre>
	 * # this is a comment
	 * MyProperty = 1.23
	 * AnotherProperty = Hello, world
	 * </pre>
	 *
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class Properties : public Hashtable< String , String >
	{
	public:
		/** Creates an emptry property list. */
		Properties();

		/** 
		 * Reads property list from the stream. 
		 * @exception IOException
		 */
		void load( InputStream* in );

		/**
		 * Writes property list to the stream.
		 * @exception IOException
		 */
		void store( OutputStream* out );
		
	private:
		uint16_t	peekChar( Reader* reader, int* chbuf );
		uint16_t 	readChar( Reader* reader, int* chbuf, int* line );
		String		readLine( Reader* reader, int* chbuf, int* line );
		String		readString( Reader* reader, int* chbuf, int* line );
		bool		skipWhitespace( Reader* reader, int* chbuf, int* line );
		void		writeString( Writer* writer, const String& str );
		int			hexToInt( uint16_t ch, int* err ) const;
	};
}

#endif