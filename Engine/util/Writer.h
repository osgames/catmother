#ifndef _IO_WRITER_H
#define _IO_WRITER_H

#include <util/Object.h>
#include <util/String.h>

namespace util
{
	/**
	 * Abstract base class for character stream writers.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class Writer : public Object
	{
	public:
		virtual ~Writer();

		/**
		 * Writes specified number of characters to the stream.
		 * Ignores invalid character pairs.
		 * @exception IOException
		 */
		virtual void	write( const uint16_t* buffer, long count ) = 0;

		/**
		 * Flushes the stream.
		 * @exception IOException
		 */
		virtual void	flush() = 0;

		/** Closes the stream. */
		virtual void	close() = 0;

		/** Returns name of the writer. */
		virtual String	toString() const = 0;

	protected:
		Writer();

	private:
		Writer( const Writer& );
		Writer& operator=( const Writer& );
	};
}

#endif