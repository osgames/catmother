#include "Short.h"
#include "NumberParse.h"
#include <limits>

#undef max
#undef min

namespace util
{
	short	Short::MAX_VALUE = std::numeric_limits<short>::max();
	short	Short::MIN_VALUE = std::numeric_limits<short>::min();

	short Short::parseShort( const String& str )
	{
		return NumberParse<short>::parse( str );
	}
}