#ifndef _IO_UNSUPPORTEDENCODINGEXCEPTION_H
#define _IO_UNSUPPORTEDENCODINGEXCEPTION_H

#include <util/IOException.h>

namespace util
{
	/**
	 * Thrown if specified character encoding is not supported.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class UnsupportedEncodingException : public IOException
	{
	public:
		UnsupportedEncodingException( const Format& msg  ) : IOException(msg) {}
	};
}

#endif