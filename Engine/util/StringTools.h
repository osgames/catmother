#ifndef _LANG_STRINGTOOLS_H
#define _LANG_STRINGTOOLS_H

#include <algorithm>
#include <vector>
#include <string>

namespace util
{
	typedef std::string String_t;
	typedef std::vector<String_t> StringList;

	extern String_t GetStringFileExtension( const String_t& vFileName );
	extern String_t GetStringFileName( const String_t& vPath );
	extern StringList SplitString( const String_t& line, const char* delims = " \r\n\t", bool first = false, bool skiprep = true );
	extern String_t CombinePath( const String_t& first, const String_t& second );
	extern void SplitPath( const String_t& line, String_t& path, String_t& name, char delim = 0 );
	extern StringList TokenizeString( const String_t& line, const char* delims = " \r\t\n" );
	extern String_t TrimString( const String_t& line, const char* delims = " \t\r\n" );
	extern String_t LowerString( const String_t& vIn );
	extern int StringToInt( const String_t& vString );
	extern String_t IntToString( int vValue );
	extern float StringToFloat( const String_t& vString );
	extern String_t FloatToString( float vValue );
	extern bool StringToBool( const String_t& vString );
	extern String_t BoolToString( bool vValue );
	extern String_t PointerToString( const void* vValue );
}

#endif