#include "raw.h"
#include "Group.h"
#include "Group_t.h"
#include "GroupItem_t.h"


//-----------------------------------------------------------------------------

MEM_API void* mem_allocate( int n, const char* file, int line )
{
	void* group = mem_Group_create( file, line );
	void* p = mem_Group_allocate( group, n );
	mem_Group_release( group );
	return p;
}

MEM_API void mem_free( void* p )
{
	if ( p )
	{
		GroupItem_t* item = (GroupItem_t*)( (char*)p - BLOCK_HEADER_SIZE );
		mem_Group_free( item->group, p, item->size );
	}
}

#include <malloc.h>

//-----------------------------------------------------------------------------

static uint32_t framealloc_watermark;
static uint32_t framealloc_size;
static uint8_t*  framealloc_data;

MEM_API void        mem_framealloc_set(uint32_t size)
{
	if (framealloc_data)
	{
		free((uint8_t*)framealloc_data);
		framealloc_size = 0;
	}

	if (size > 0)
	{
		framealloc_data = (uint8_t*)malloc(size);
		framealloc_size = size;
	} else
		framealloc_data = 0;

	framealloc_watermark = 0;
}

MEM_API uint32_t    mem_framealloc_getwatermark()
{
	return framealloc_watermark;
}

MEM_API void        mem_framealloc_setwatermark(uint32_t pos)
{
	framealloc_watermark = pos;
}

MEM_API void*       mem_framealloc_alloc(uint32_t size)
{
	framealloc_watermark = framealloc_watermark + size;
	if (framealloc_watermark > framealloc_size) {
		framealloc_watermark = framealloc_size;
		return 0;
	}
	return framealloc_data + framealloc_watermark;
}

