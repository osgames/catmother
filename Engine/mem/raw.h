#ifndef _MEM_RAW_H
#define _MEM_RAW_H

#include <util/stdint.h>

#ifdef MEM_EXPORTS
	#ifdef __cplusplus
	#define MEM_API extern "C" __declspec(dllexport)
	#else
	#define MEM_API __declspec(dllexport)
	#endif
#else
	#ifdef __cplusplus
	#define MEM_API extern "C" __declspec(dllimport)
	#else
	#define MEM_API __declspec(dllimport)
	#endif
#endif

/** 
 * Allocates n byte memory block. 
 * Use mem_alloc macro to get automatic filename and line number.
 * @see mem_free
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
MEM_API void* mem_allocate( int n, const char* file, int line );

/** 
 * Frees memory block allocated with mem_allocate. 
 * @see mem_allocate
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
MEM_API void mem_free( void* p );

/**
  * Allocates the frame allocator buffer.
  * @param size Size of buffer.
  * @author James Urquhart (jamesu@gmail.com)
  */
MEM_API void mem_framealloc_set(uint32_t size);

/**
  * Grabs the current watermark position in the
  * allocator buffer.
  * @return Current watermark position.
  * @author James Urquhart (jamesu@gmail.com)
  */
MEM_API uint32_t mem_framealloc_getwatermark();

/**
  * Sets the current watermark position in the
  * allocator buffer.
  * @param pos Position in allocator buffer.
  * @author James Urquhart (jamesu@gmail.com)
  */
MEM_API void mem_framealloc_setwatermark(uint32_t pos);

/**
  * Allocates memory from the alloctor buffer.
  * Will return a pointer if there is sufficient memory
  * available (from the current watermark position).
  * Otherwise, the operation will fail and 0 is returned.
  * @param size Size of allocation.
  * @return Pointer to memory, if available.
  * @author James Urquhart (jamesu@gmail.com)
  */
MEM_API void* mem_framealloc_alloc(uint32_t size);


#if defined(NDEBUG) && !defined(MEM_EXPORTS)
#	include <malloc.h>
#	define mem_allocate( BYTES, PARAM, PARAM2 ) malloc(BYTES); (PARAM); (PARAM2);
#	define mem_free( PTR ) free(PTR)
#	define mem_alloc( BYTES ) malloc(BYTES);
#else
#	define MEM_ALLOCATE( BYTES ) mem_allocate(BYTES,__FILE__,__LINE__)
#	define mem_alloc MEM_ALLOCATE
#endif

// Operations in-place

#ifdef __cplusplus
template <class T>
inline T* constructInPlace(T* p)
{
   return new(p) T;
}

template <class T>
inline T* constructInPlace(T* p, const T* copy)
{
   return new(p) T(*copy);
}

template <class T>
inline void destructInPlace(T* p)
{
   p->~T();
}
#endif

#endif