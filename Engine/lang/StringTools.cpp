#include <lang/Exception.h>
#include "StringTools.h"

/** local variable is initialized but not referenced. */
#pragma warning( disable : 4189 ) 

namespace lang
{
	inline bool ischar( char c, const char* chars )
	{
		int x;
		for ( x = 0; chars[x] != 0; x++ )
		{
			if ( chars[x] == c )
				return true;
		}

		return false;
	}

	StringList TokenizeString( const String_t& line, const char* delims )
	{
		StringList tokens;
		String_t tmp;

		// Get rid of any extra white space
		String_t stripped = TrimString( line );

		bool inDelim = false;

		int size = (int)stripped.size( );
		char str = 0;

		int x;
		for ( x = 0; x < size; x++ )
		{
			// Get next character
			char c = stripped[x];
		
			if ( (c == '\"') || (c == '\'') )
			{
				// Push any existing token
				if ( tmp != "" )
				{
					tokens.push_back( tmp );
					tmp = "";
				}

				// Read entire string

				x++;
				while ( (x < size) && (stripped[x] != c)  )
					tmp += stripped[x++];

				// Push string

				tokens.push_back( tmp );
				tmp = "";

				inDelim = true;
			}
			else if ( ischar( c, delims ) )
			{
				if ( inDelim == false )
				{
					tokens.push_back( tmp );
					tmp = "";
				}

				inDelim = true;
			}
			else if ( c == '#' )
			{
				// Found a comment... break from line
				break;
			}
			else
			{
				inDelim = false;
				tmp += c;
			}
		}

		if ( tmp != "" )
			tokens.push_back( tmp );

		return tokens;
	}

	StringList SplitString( const String_t& line, const char* delims, bool first, bool skiprep )
	{
		if ( delims == 0 )
			delims = " \r\n\t";

		StringList rv;

		String_t::const_iterator i = line.begin( );
		String_t::const_iterator j = line.begin( );

		if( line == "" )
		{
			rv.push_back( "" );
			return rv;
		}

		while( j != line.end( ) )
		{
			// Skip over repetitive delimiters.
			while( (j != line.end( )) && ischar( (*j), delims ) )
			{
				++j;
				if ( skiprep == false )
					break;
			}

			// Anchor at first non-delimiter.
			i = j;

			// Find the next delimiter.
			while( (j != line.end( )) && !ischar( (*j), delims ) )
			{
				++j;
			}
	 
			// Save the string you've found.
			rv.push_back( String_t( i, j ) );

			if ( first )
			{
				if ( j != line.end( ) )
					rv.push_back( String_t( j+1, line.end( ) ) );

				return rv;
			}
		}

		return rv;
	}

	String_t GetStringFileName( const String_t& vPath )
	{
		StringList parts = SplitString( vPath, "." );
		if ( parts.size( ) <= 0 )
			return "";

		return parts[0];
	}

	int rfind( const String_t& line, char c )
	{
		int size = (int)line.size( );

		int index;
		for ( index = size-1; index >= 0; index-- )
		{
			if ( line[index] == c )
				return index;
		}

		return -1;
	}

	void SplitPath( const String_t& line, String_t& path, String_t& name, char delim )
	{
		if ( delim == 0 )
		{
			delim = '/';
		}

		bool found = false;
		int size = (int)line.size( );

		// Get index of last slash
		int index = rfind( line, delim );

		// See if a delimeter was found
		if ( index < 0 )
		{
			path = "";
			name = line;
			return;
		}

		// Absolute path with single slash
		if ( index == 0 )
		{
			path = "/";
			name = line.c_str( )+1;
			return;
		}

		int y;

		// Get everything before delimeter

		for ( y = 0; y < index; y++ )
			path += line[y];

		// Get everything after delimeter

		for ( y = index+1; y < size; y++ )
			name += line[y];
	}

	String_t CombinePath( const String_t& first, const String_t& second )
	{
		String_t tmp;

		if ( TrimString( second ) == "" )
			return first;

		tmp = first + "/" + second;
		return tmp;
	}

	String_t TrimString( const String_t& line, const char* delims )
	{
		String_t result = line;
		String_t::size_type index = result.find_last_not_of( delims );
	  
		if( index != String_t::npos )
			result.erase( ++index );

		index = result.find_first_not_of( delims );
		if( index != String_t::npos )
			result.erase( 0, index );
		else
			result.erase( );
	  
		return result;
	}

	String_t GetStringFileExtension( const String_t& vFileName )
	{
		StringList parts = SplitString( vFileName, "." );
		if ( parts.size( ) <= 1 )
			return "";

		return LowerString( parts[parts.size( )-1] );
	}

	String_t LowerString( const String_t& vIn )
	{
		String_t tmp = vIn;

		// Convert string to lower case
		std::transform( tmp.begin( ), tmp.end( ), tmp.begin( ), tolower );
		return tmp;
	}

	bool StringToBool( const String_t& vString )
	{
		return ( LowerString( vString ) == "true" ) ? true : false;
	}

	String_t BoolToString( bool v )
	{
		return ( v == true ) ? "true" : "false";
	}

	int StringToInt( const String_t& vString )
	{
		return atoi( vString.c_str( ) );
	}

	String_t IntToString( int v )
	{
		static char buffer[128];
		sprintf( buffer, "%d", v );
		return buffer;
	}

	float StringToFloat( const String_t& vString )
	{
		float i = 0.0f;
		sscanf( vString.c_str( ), "%f", &i );

		return i;
	}

	String_t FloatToString( float v )
	{
		static char buffer[128];
		sprintf( buffer, "%f", v );
		return buffer;
	}

	String_t PointerToString( const void* vValue )
	{
		static char buffer[128];
		sprintf( buffer, "%p", vValue );
		return buffer;
	}
}