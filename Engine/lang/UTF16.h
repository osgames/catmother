#ifndef _LANG_UTF16_H
#define _LANG_UTF16_H

#include <lang/stdint.h>

namespace lang
{
	/**
	 * Low-level UTF-16 encoding functionality.
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 */
	class UTF16
	{
	public:
		/** Returns true if the character is first surrogate. */
		static bool isFirstSurrogate( uint16_t ch )
		{
			return ( uint32_t(ch) >= 0xD800 && uint32_t(ch) <= 0xDBFF );}

		/** Returns true if the character is second surrogate. */
		static bool isSecondSurrogate( uint16_t ch )
		{
			return ( uint32_t(ch) >= 0xDC00 && uint32_t(ch) <= 0xDFFF );}

		/** Returns code point from surrogate pair. */
		static uint32_t makeCodePoint( uint16_t ch1, uint32_t ch2 )
		{
			return ( ((uint32_t(ch1)-0xD800) << 10) + (uint32_t(ch2)-0xDC00) + 0x10000 );
		}

		/** 
		 * Appends code point to character sequence.
		 * @return Pointer to sequence after added character(s).
		 */
		static uint16_t* append( uint16_t* cz, uint32_t cp )
		{
			if ( cp >= 0x10000 )
			{
				*cz++ = uint16_t( ((cp-0x10000)>>10) + 0xD800 );
				*cz++ = uint16_t( ((cp-0x10000)&1023) + 0xDC00 );
			}
			else
			{
				*cz++ = uint16_t( cp );
			}
			return cz;
		}
	};
}

#endif