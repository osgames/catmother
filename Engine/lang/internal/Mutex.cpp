#include "Mutex.h"

// select atomic addition operation implementation
#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32 && CATMOTHER_COMPILER == CATMOTHER_COMPILER_MSVC
#	define WIN32_LEAN_AND_MEAN
#	include <windows.h>
#	define ATOMICADD_INTELWIN32
#	pragma warning( disable : 4035 ) // 'no return value'
#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
#	define ATOMICADD_WIN32
#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
#	include <pthread.h>
#	define ATOMICADD_PTHREADS
	static pthread_mutex_t s_refCountMutex = PTHREAD_MUTEX_INITIALIZER;
#else
#	error Current platform does not have atomic addition implemented.
#endif

namespace lang
{
	#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
		Mutex::Mutex( )
		{
			InitializeCriticalSection( &mCritSection );
		}

		Mutex::~Mutex( )
		{
			DeleteCriticalSection( &mCritSection );
		}

		void Mutex::lock( )
		{
			EnterCriticalSection( &mCritSection );
		}

		void Mutex::unlock( )
		{
			LeaveCriticalSection( &mCritSection );
		}

		bool Mutex::request( )
		{
			return (TryEnterCriticalSection( &mCritSection ) == 0);
		}

		void Mutex::incrementRC( long* value )
		{
			#if defined(ATOMICADD_INTELWIN32)
				__asm
				{
					mov ebx, value
					mov eax, 1
					lock xadd dword ptr [ebx], eax
					inc eax
				}
			#elif defined(ATOMICADD_WIN32)
				InterlockedIncrement( value );
			#elif defined(ATOMICADD_PTHREADS)
				pthread_mutex_lock( &s_refCountMutex );
				long v = ++*x;
				pthread_mutex_unlock( &s_refCountMutex );
				return v;
			#endif
		}

		long Mutex::decrementRC( long* value )
		{
			#if defined(ATOMICADD_INTELWIN32)
				__asm
				{
					mov ebx, value
					mov eax, -1
					lock xadd dword ptr [ebx], eax
					dec eax
				}
			#elif defined(ATOMICADD_WIN32)
				return InterlockedDecrement( value );
			#elif defined(ATOMICADD_PTHREADS)
				pthread_mutex_lock( &s_refCountMutex );
				long v = --*x;
				pthread_mutex_unlock( &s_refCountMutex );
				return v;
			#endif
		}

		long Mutex::testAndSet( long* value, long newValue )
		{
			#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
				return InterlockedExchange( value, newValue );
			#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
				pthread_mutex_lock( &s_refCountMutex );
				long v = *value;
				*value = newValue;
				pthread_mutex_unlock( &s_refCountMutex );
				return v;
			#endif
		}
	#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
		Mutex::Mutex( )
		{
			pthread_mutexattr_t mutexattr;
			pthread_mutexattr_settype(&mutexattr, PTHREAD_MUTEX_RECURSIVE_NP);
			pthread_mutex_init(&mCritSection, &mutexattr);
			pthread_mutexattr_destroy(&mutexattr);
		}

		Mutex::~Mutex( )
		{
			pthread_mutex_destroy( &mCritSection );
		}

		void Mutex::lock( )
		{
			pthread_mutex_lock(&mCritSection);
		}

		void Mutex::unlock( )
		{
			pthread_mutex_unlock(&mCritSection);
		}

		bool Mutex::request( )
		{
			return (pthread_mutex_trylock( &mCritSection ) == 0);
		}
	#else
	#	error "No Mutex implementation"
	#endif
}