#ifndef _LANG_MUTEX_H
#define _LANG_MUTEX_H

#include <config/CatMother.h>

#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
#	define _WIN32_WINNT 0x0400
#	define _WINSOCKAPI_
#	include <windows.h>
#	undef _WIN32_WINNT
#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
#	include <pthread.h>
#endif

namespace lang
{
	// Encapsulates a mutex object for thread synchronization
	class Mutex
	{
	private:

	#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
		CRITICAL_SECTION mCritSection;
	#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
		pthread_mutex_t	mCritSection;
	#endif

	public:
		Mutex( );
		~Mutex( );

		/** Locks the mutex, blocks until access is granted */
		void lock( );

		/** Unlocks the mutex */
		void unlock( );

		/** return True if the mutex was entered, false if not */
		bool request( );

		/** Atomic increment operation for reference counting. */
		static void incrementRC( long* value );

		/** Atomic decrement operation for reference counting. */
		static long decrementRC( long* value );

		/** Atomic test-and-set operation. */
		static long testAndSet( long* value, long newValue=1 );
	};

	class MutexScopeLock
	{
	private:
		Mutex* mMutex;

	public:
		inline MutexScopeLock( )
		{
			mMutex = 0;
		}

		inline MutexScopeLock( Mutex* vMutex ) : mMutex( vMutex )
		{
			if ( mMutex )
				mMutex->lock( );
		}

		inline MutexScopeLock( Mutex& vMutex ) : mMutex( &vMutex )
		{
			mMutex->lock( );
		}

		inline ~MutexScopeLock( )
		{
			if ( mMutex )
				mMutex->unlock( );
		}
	};
}

#endif
