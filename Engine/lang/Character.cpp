#include "Character.h"
#include <ctype.h>

namespace lang
{
	bool Character::isDigit( uint32_t cp )
	{
		if ( cp < uint32_t(0x80) )
			return 0 != isdigit( (char)cp );
		else
			return false;
	}

	bool Character::isLetter( uint32_t cp )
	{
		if ( cp < uint32_t(0x80) )
			return 0 != isalpha( (char)cp );
		else
			return false;
	}

	bool Character::isLetterOrDigit( uint32_t cp )
	{
		return isLetter(cp) || isDigit(cp);
	}

	bool Character::isLowerCase( uint32_t cp )
	{
		if ( cp < uint32_t(0x80) )
			return 0 != islower( (char)cp );
		else
			return false;
	}

	bool Character::isUpperCase( uint32_t cp )
	{
		if ( cp < uint32_t(0x80) )
			return 0 != isupper( (char)cp );
		else
			return false;
	}

	bool Character::isWhitespace( uint32_t cp )
	{
		if ( cp < uint32_t(0x80) )
			return 0 != isspace( (char)cp );
		else
			return false;
	}
}