#include <config/CatMother.h>
#include "DynamicLinkLibrary.h"
#include <lang/Array.h>
#include <lang/Exception.h>
#include <assert.h>

#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_WIN32
#	define WIN32_LEAN_AND_MEAN
#	include <Windows.h>
#	define PLUGIN_HANDLE HINSTANCE
#	define PLUGIN_LOAD( a ) LoadLibrary( a )
#	define PLUGIN_GETSYM( a, b ) GetProcAddress( a, b )
#	define PLUGIN_UNLOAD( a ) !FreeLibrary( a )
#	define PLUGIN_EXTENSION ".dll"
#elif CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
#	include <dlfcn.h>
#   define PLUGIN_HANDLE void*
#   define PLUGIN_LOAD( a ) dlopen( a, RTLD_NOW )
#   define PLUGIN_GETSYM( a, b ) dlsym( a, b )
#   define PLUGIN_UNLOAD( a ) dlclose( a )
#	define PLUGIN_EXTENSION ".so"
#else
#	pragma error "UNKNOWN PLATFORM."
#endif

namespace lang
{
	class DynamicLinkLibrary::DynamicLinkLibraryImpl : public Object
	{
	public:
		DynamicLinkLibraryImpl( const char* name );
		~DynamicLinkLibraryImpl();

		void	close();
		void*	getProcAddress( const char* name ) const;

	private:
		PLUGIN_HANDLE m_dll;

		DynamicLinkLibraryImpl();
		DynamicLinkLibraryImpl( const DynamicLinkLibraryImpl& );
		DynamicLinkLibraryImpl& operator=( const DynamicLinkLibraryImpl& );
	};

	DynamicLinkLibrary::DynamicLinkLibraryImpl::DynamicLinkLibraryImpl( const char* name )
	{
		char fname[1024];
		
		#if CATMOTHER_PLATFORM == CATMOTHER_PLATFORM_LINUX
			memset( fname, 0, sizeof(fname) );
		#endif

		strncpy( fname, name, 1000 );
		fname[sizeof(fname)-10] = 0;
		
		#ifdef _DEBUG
			strcat( fname, "d" );
		#endif

		strcat( fname, PLUGIN_EXTENSION );

		m_dll = LoadLibrary( fname );
		if ( !m_dll )
			throw Exception( Format("Failed to load plugin: {0}", fname) );
	}

	DynamicLinkLibrary::DynamicLinkLibraryImpl::~DynamicLinkLibraryImpl()
	{
		close();
	}

	void DynamicLinkLibrary::DynamicLinkLibraryImpl::close()
	{
		if ( m_dll )
		{
			PLUGIN_UNLOAD( m_dll );
			m_dll = 0;
		}
	}

	void* DynamicLinkLibrary::DynamicLinkLibraryImpl::getProcAddress( const char* name ) const
	{
		void* addr = 0;
		if ( m_dll )
			addr = PLUGIN_GETSYM( m_dll, name );
		return addr;
	}

	DynamicLinkLibrary::DynamicLinkLibrary( const lang::String& name )
	{
		char buffer[512];
		name.getBytes( buffer, sizeof(buffer), "ASCII-7" );
		m_this = new DynamicLinkLibraryImpl( buffer );
	}

	DynamicLinkLibrary::~DynamicLinkLibrary()
	{
	}

	void* DynamicLinkLibrary::getProcAddress( const lang::String& name ) const
	{
		char buffer[512];
		name.getBytes( buffer, sizeof(buffer), "ASCII-7" );
		return m_this->getProcAddress( buffer );
	}

	void DynamicLinkLibrary::close()
	{
		m_this->close();
	}
}