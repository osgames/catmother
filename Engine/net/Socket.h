#ifndef _LANG_SOCKET_H
#define _LANG_SOCKET_H

#include <net/SocketException.h>
#include <util/Object.h>

namespace util
{
	class String;
}

namespace net
{
	class Socket : public util::Object
	{
	private:
		int mSocket;
		
	public:
		Socket( );

		virtual ~Socket( );

		// Sets whether or not the socket should block
		void setBlocking( bool vWhich );

		// Connects to the specified address and port
		void connect( const char* vName, size_t vPort );

		// Closes the socket
		void close( );

		// Sends data through the socket. Throws a SocketErrorException if an
		// error occurs. May return 0 if the socket is non-blocking
		size_t send( const void* vData, size_t vSize );

		// Receives data through the socket. Throws a SocketErrorException if
		// an error occurs. May return 0 if the socket is non-blocking
		size_t receive( void* vData, size_t vSize );
	};

	// Initialises the networking system.  Call this before using Socket
	void InitialiseNetworking( );
}

#endif
