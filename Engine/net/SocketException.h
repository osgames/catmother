#ifndef _SOCKET_EXCEPTION_H__
#define _SOCKET_EXCEPTION_H__

#include <util/Exception.h>

namespace net
{
	class SocketErrorException : public util::Exception
	{
	public:
		SocketErrorException( const util::Format& msg ) : util::Exception(msg)
		{
		
		}
	};
}

#endif