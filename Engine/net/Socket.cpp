#include <windows.h>
#include <winsock.h>
#include <fcntl.h>
#include <net/Socket.h>
#include <util/String.h>
#include <util/Object.h>

namespace net
{
	Socket::Socket( ) : mSocket( -1 )
	{

	}

	Socket::~Socket( )
	{
		close( );
	}

	void Socket::setBlocking( bool vWhich )
	{
		ULONG v = ( vWhich ? 0 : 1 );
		ioctlsocket( mSocket, FIONBIO, &v );
	}

	void Socket::connect( const char* vName, size_t vPort )
	{
		struct sockaddr_in their_addr;
		struct hostent* hostEnt;

		// Make sure socket is closed
		close( );

		// Resolve the host name
		hostEnt = gethostbyname( vName );

		if ( hostEnt == 0 )
			throw SocketErrorException( util::Format("Unable to resolve host name '{0}'", vName ) );

		// Fill in address structure
		their_addr.sin_family = AF_INET;
		their_addr.sin_port = htons( vPort );
		their_addr.sin_addr = *((struct in_addr *)hostEnt->h_addr);

		memset( their_addr.sin_zero, '\0', sizeof( their_addr.sin_zero ) );

		// Create the socket
		mSocket = socket( PF_INET, SOCK_STREAM, 0 );
		if ( mSocket == -1 )
			throw SocketErrorException( util::Format("Unable to create socket object") );

		// Establish connection
		int res = ::connect( mSocket, (struct sockaddr*)&their_addr, sizeof( their_addr ) );
		if ( res == -1 )
			throw SocketErrorException( util::Format("Unable to connect to host '{0}'", vName) );
	}

	void Socket::close( )
	{
		if ( mSocket > -1 )
		{
			::closesocket( mSocket );

			mSocket = -1;
		}
	}

	size_t Socket::send( const void* vData, size_t vSize )
	{
		int res = ::send( mSocket, (const char*)vData, vSize, 0 );
		if ( res <= 0 )
		{
			int code = WSAGetLastError( );
			if ( code == WSAEWOULDBLOCK )
				return 0;
			throw SocketErrorException( util::Format("Resource temporarily unavailable.") );
		}

		return (size_t)res;
	}

	size_t Socket::receive( void* vData, size_t vSize )
	{
		int res = ::recv( mSocket, (char*)vData, vSize, 0 );
		if ( res <= 0 )
		{
			int code = WSAGetLastError( );
			if ( code == WSAEWOULDBLOCK )
				return 0;

			throw SocketErrorException( util::Format("Resource temporarily unavailable.") );
		}


		return (size_t)res;
	}

	void InitialiseNetworking( )
	{
		WORD versionRequested = MAKEWORD( 1, 1 );

		WSADATA wsaData;
		if ( WSAStartup( versionRequested, &wsaData ) )
			throw SocketErrorException( util::Format("WSAStartup failed") );

		if ( (LOBYTE( wsaData.wVersion ) != 1) || (HIBYTE( wsaData.wVersion ) != 1) )
		{
		   WSACleanup ( );
		   throw SocketErrorException( util::Format("Bad WinSock version") );
		}
	}
}