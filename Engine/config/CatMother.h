#ifndef _CatMother_H__
#define _CatMother_H__

#define CATMOTHER_PLATFORM_WIN32 1
#define CATMOTHER_PLATFORM_LINUX 2
#define CATMOTHER_PLATFORM_SONY  3
#define CATMOTHER_PLATFORM_XBOX  4

#define CATMOTHER_COMPILER_MSVC 1
#define CATMOTHER_COMPILER_GNUC 2

#define CATMOTHER_ARCHITECTURE_32 1
#define CATMOTHER_ARCHITECTURE_64 2

/* Find the arch type */
#if defined(__x86_64__) || defined(_M_X64) || defined(__ia64__)
#   define CATMOTHER_ARCH_TYPE CATMOTHER_ARCHITECTURE_64
#else
#   define CATMOTHER_ARCH_TYPE CATMOTHER_ARCHITECTURE_32
#endif

/* Sets the compiler and version. */
#if defined( _MSC_VER )
#   define CATMOTHER_COMPILER CATMOTHER_COMPILER_MSVC
#   define CATMOTHER_COMP_VER _MSC_VER
#else
#   pragma error "UNKNOWN COMPILER, PLEASE CHECK SETTINGS!"
#endif

/* See if we can use __forceinline or if we need to use __inline instead */
#if CATMOTHER_COMPILER == CATMOTHER_COMPILER_MSVC
#	define FORCEINLINE __forceinline
#else
#   define FORCEINLINE __inline
#endif

/* Finds the current platform */
#if defined( __WIN32__ ) || defined( _WIN32 )
#   define CATMOTHER_PLATFORM CATMOTHER_PLATFORM_WIN32

// Win32 compilers use _DEBUG for specifying debug builds.
#   ifdef _DEBUG
#       define CATMOTHER_DEBUG_MODE 1
#   else
#       define CATMOTHER_DEBUG_MODE 0
#   endif
#else
#   define CATMOTHER_PLATFORM CATMOTHER_PLATFORM_LINUX
#endif

#endif