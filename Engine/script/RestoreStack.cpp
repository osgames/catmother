#include "RestoreStack.h"
#include <script/VM.h>
extern "C"
{
#include <lua.h>
}


//-----------------------------------------------------------------------------

namespace script
{


RestoreStack::RestoreStack( VM* vm ) :
	m_lua( vm->lua() ),
	m_top( lua_gettop(m_lua) )
{
}

RestoreStack::RestoreStack( lua_State* lua ) :
	m_lua( lua ),
	m_top( lua_gettop(lua) )
{
}

RestoreStack::~RestoreStack()
{
	lua_settop( m_lua, m_top );
}


} // script
