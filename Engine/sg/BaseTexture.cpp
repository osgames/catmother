#include "Context.h"
#include "BaseTexture.h"
#include "CubeTexture.h"
#include "TextureCache.h"
#include "Texture.h"
#include <util/InputStream.h>
#include <pix/SurfaceFormat.h>


//-----------------------------------------------------------------------------

namespace sg
{


static TextureCache		s_textureCache;

//-----------------------------------------------------------------------------

BaseTexture::BaseTexture() :									
	m_name("") 
{
}

BaseTexture::~BaseTexture()
{
}

/** Sets the name of the texture. */
void BaseTexture::setName( const util::String& name )		
{ 
	m_name = name; 
}
	
/** Returns the name of the texture. */
const util::String&	BaseTexture::name() const		
{ 
	return m_name; 
}

TextureCache& BaseTexture::getTextureCache()
{
	return s_textureCache;
}


} // sg
