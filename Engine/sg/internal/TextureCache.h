#ifndef _SG_TEXTURECACHE_H
#define _SG_TEXTURECACHE_H


#include <gd/Texture.h>
#include <gd/CubeTexture.h>
#include <util/String.h>
#include <util/Hashtable.h>


namespace gd {
	class GraphicsDriver;}

namespace util {
	class InputStream;}

namespace util {
	class String;}


namespace sg
{


/** 
 * Class for caching texture loading. 
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class TextureCache :
	public util::Object
{
public:
	///
	TextureCache();

	/** Releases cached textures. */
	void				flushTextures();

	/** Loads a texture from stream if not in cache. */
	gd::Texture*		loadTexture( util::InputStream* in, const util::String& name, 
							gd::GraphicsDriver* drv, gd::GraphicsDevice* dev );

	/** Loads a cube texture from stream if not in cache. */
	gd::CubeTexture*	loadCubeTexture( util::InputStream* in, const util::String& name, 
							gd::GraphicsDriver* drv, gd::GraphicsDevice* dev );

	/** Loads multi-face cubetexture (as above). */
	gd::CubeTexture*	loadCubeTexture( util::InputStream* in1, util::InputStream* in2, 
						util::InputStream* in3, util::InputStream* in4, 
						util::InputStream* in5, util::InputStream* in6,
						const util::String& name, 
						gd::GraphicsDriver* drv, gd::GraphicsDevice* dev );
	
	/** Enables/disables texture downscaling. */
	void				setDownScaling( bool enabled );

	/** Sets default bit depth for loaded textures. */
	void				setDefaultBitDepth( int bits );

	/** Returns (approximate) number of bytes device memory used by the textures. */
	long				textureMemoryUsed() const;

private:
	util::Hashtable<util::String,P(gd::BaseTexture)>	m_textures;
	bool												m_downScaling;
	int													m_bitDepth;

	gd::Texture*		loadTextureImpl( util::InputStream* in, const util::String& name, 
							gd::GraphicsDriver* drv, gd::GraphicsDevice* dev );
	gd::CubeTexture*	loadCubeTextureImpl( util::InputStream* in, const util::String& name, 
							gd::GraphicsDriver* drv, gd::GraphicsDevice* dev );

	TextureCache( const TextureCache& );
	TextureCache& operator=( const TextureCache& );
};


} // sg


#endif // _SG_TEXTURECACHE_H
