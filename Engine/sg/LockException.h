#ifndef _SG_LOCKEXCEPTION_H
#define _SG_LOCKEXCEPTION_H


#include <util/Exception.h>


namespace sg
{


/** 
 * Throw if graphics device locking failed. 
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class LockException :
	public util::Exception
{
public:
	LockException( const util::String& typeName )	: util::Exception( util::Format("Failed to lock {0}", typeName) ) {}
};


} // sg


#endif // _SG_LOCKEXCEPTION_H
