#ifndef _SGU_NODEGROUPSET_H
#define _SGU_NODEGROUPSET_H


#include <util/String.h>
#include <util/Hashtable.h>


namespace sg {
	class Node;}


namespace sgu
{



/** 
 * Set of (animated) node groups. 
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class NodeGroupSet :
	public util::Object
{
public:
	/** Creates empty set. */
	NodeGroupSet();

	///
	~NodeGroupSet();

	/** Adds a group of nodes. */
	void		addGroup( const util::String& group, sg::Node* root );

	/** 
	 * Returns root node of a group. 
	 * Assumes that all nodes of a same group are in same hierarchy.
	 */
	sg::Node*	getGroup( const util::String& group );

	/** Returns named node in specified group (if any). */
	sg::Node*	getNode( const util::String& group, const util::String& name );

	/** Returns true if the group exist. */
	bool		hasGroup( const util::String& group ) const;

private:
	typedef util::Hashtable<util::String,P(sg::Node)> NodeSetType;
	typedef util::HashtableIterator<util::String,P(sg::Node)> NodeSetIteratorType;

	util::Hashtable< util::String, P(NodeSetType) > m_groups;

	NodeGroupSet( const NodeGroupSet& );
	NodeGroupSet& operator=( const NodeGroupSet& );
};


} // sgu


#endif // _SGU_NODEGROUPSET_H
