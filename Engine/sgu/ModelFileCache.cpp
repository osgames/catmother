#include "ModelFileCache.h"
#include <util/FileInputStream.h>
#include <util/InputStreamArchive.h>
#include <sgu/ModelFile.h>
#include <util/String.h>
#include <math/Vector3.h>
#include <util/Hashtable.h>
#include <assert.h>

using namespace math;
using namespace util;

//-----------------------------------------------------------------------------

namespace sgu
{


class ModelFileCache::ModelFileCacheImpl :
	public util::Object
{
public:
	Hashtable< String, P(ModelFile) >	files;
	P(InputStreamArchive)				zip;

	ModelFileCacheImpl() :
		files( Allocator< HashtablePair<String,P(ModelFile)> >(__FILE__,__LINE__) ),
		zip(0)
	{
	}
};

//-----------------------------------------------------------------------------

ModelFileCache::ModelFileCache( InputStreamArchive* zip )
{
	assert( zip );

	m_this			= new ModelFileCacheImpl;
	m_this->zip		= zip;
}

ModelFileCache::~ModelFileCache()
{
}

ModelFile* ModelFileCache::getByName( const String& name,
	const String* boneNames, int bones, const pix::Colorf& ambient, int loadFlags )
{
	String fname = name.toLowerCase();
	P(ModelFile) model = m_this->files[fname];
	if ( !model )
	{
		model = new ModelFile( name, boneNames, bones, ambient, m_this->zip, this, loadFlags );
		m_this->files[fname] = model;
	}
	return model;
}

void ModelFileCache::clear()
{
	m_this->files.clear();
}


} // sgu
