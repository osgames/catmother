#ifndef _MATH_LERP_H
#define _MATH_LERP_H


namespace math
{
	/** 
	 * Linear interpolation of clamped range [a,b]. 
	 * @author Jani Kajala (jani.kajala@helsinki.fi)
	 * @rewrite Gordon Smith (smizmar@live.co.uk)
	 */
	template<typename T1, typename T2>
	inline T1 lerp(const T1 &start, const T1 &end, const T2 &fraction)
	{
		return (end - start) * fraction + start;
	}
}

#endif