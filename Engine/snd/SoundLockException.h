#ifndef _SND_SOUNDLOCKEXCEPTION_H
#define _SND_SOUNDLOCKEXCEPTION_H


#include <util/Exception.h>


namespace snd
{


/** 
 * Throw if sound device locking failed. 
 * @author Jani Kajala (jani.kajala@helsinki.fi)
 */
class SoundLockException :
	public util::Exception
{
public:
	SoundLockException( const util::String& typeName )	: util::Exception( util::Format("Failed to lock {0}", typeName) ) {}
};


} // snd


#endif // _SND_SOUNDLOCKEXCEPTION_H
