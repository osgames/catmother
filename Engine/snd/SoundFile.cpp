#include "SoundFile.h"
#include <util/IOException.h>
#include <util/InputStream.h>
#include <util/DataInputStream.h>
#include <util/InputStreamArchive.h>
#include <util/Debug.h>
#include <util/TempAllocator.h>
#include <string.h>
#include <util/stdint.h>

using namespace util;

namespace snd
{
struct RIFFChunk
{
	char		id[4];
	uint32_t	size;	// size of data
	// data...
};

struct WaveFormatHeader
{ 
	uint16_t	formatTag;
	uint16_t	channels;
	uint32_t	samplesPerSec;
	uint32_t	avgBytesPerSec;
	uint16_t	blockAlign;
}; 

/** 
 * Finds sub RIFF chunk from current chunk.
 * @param in Input stream.
 * @param end Current chunk end.
 * @param id Id of chunk to find.
 * @param chunk [out] Receives found sub chunk info.
 */
static void findChunk( DataInputStream* in, long end, const char* id, RIFFChunk* chunk )
{
	while ( in->size() < end )
	{
		long bytes = in->read( chunk, sizeof(*chunk) );
		if ( bytes != sizeof(*chunk) )
			throw IOException( Format("Failed to read {0} chunk from {1}", id, in->toString()) );

		if ( !memcmp(id,chunk->id,4) )
			break;
		
		in->skip( chunk->size );
	}

	if ( memcmp(id,chunk->id,4) )
		throw IOException( Format("Failed to read {0} chunk from {1}", id, in->toString()) );
}

//-----------------------------------------------------------------------------

SoundFile::SoundFile( const String& name, InputStreamArchive* arch ) :
	m_in( arch->getInputStream( name ) ),
	m_dataIn( new DataInputStream(m_in) ),
	m_format(0,0,0),
	m_dataBegin(0),
	m_dataEnd(0)
{
	DataInputStream& in = *m_dataIn;

	// find RIFF chunk
	RIFFChunk ckRIFF;
	findChunk( &in, in.available(), "RIFF", &ckRIFF );
	long endRIFF = in.size() + ckRIFF.size;

	// form WAVE
	char formWAVE[4];
	in.readFully( formWAVE, 4 );
	if ( memcmp(formWAVE,"WAVE",4) )
		throw IOException( Format("Failed to read WAVE form from {0}", name) );

	// find fmt chunk
	RIFFChunk ckfmt;
	findChunk( &in, endRIFF, "fmt ", &ckfmt );
	long endfmt = in.size() + ckfmt.size;

	// read fmt data
	TempAllocator fa;
	uint8_t *fmtdata = (uint8_t*)fa.alloc(ckfmt.size);
	in.readFully( fmtdata, ckfmt.size );
	const WaveFormatHeader* wavefmt = reinterpret_cast<const WaveFormatHeader*>( fmtdata );
	m_format = SoundFormat( wavefmt->samplesPerSec, wavefmt->avgBytesPerSec / wavefmt->samplesPerSec / wavefmt->channels * 8, wavefmt->channels );

	// leave fmt chunk
	in.skip( endfmt-in.size() );

	// find data chunk
	RIFFChunk ckdata;
	findChunk( &in, endRIFF, "data", &ckdata );
	m_dataBegin = in.size();
	m_dataEnd = in.size() + ckdata.size;

	// debug info
	int samples = size() / (m_format.bitsPerSample()/8) / m_format.channels();
	float sec = (float)samples / (float)m_format.samplesPerSec();
	Debug::println( "Sound {0}: {1}kHz, {2}bits, {3} chn, {4} sec", name, m_format.samplesPerSec()/1000, m_format.bitsPerSample(), m_format.channels(), sec );
}

SoundFile::~SoundFile()
{
	if ( m_in )
		m_in->close();
}

void SoundFile::read( void* data, long bytes ) 
{
//	assert( bytes <= size() );

	m_dataIn->readFully( data, bytes );

//	assert( size() >= 0 );
}

long SoundFile::size() const 
{
	return m_dataEnd - m_dataIn->size();
}

const SoundFormat& SoundFile::format() const 
{
	return m_format;
}


} // snd
